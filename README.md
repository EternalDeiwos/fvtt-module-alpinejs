# alpine

[Alpine.js](https://github.com/alpinejs/alpine) for FoundryVTT.

# How to use

Simply install and activate the module and then include the `x-data` attribute on any `div` of one of your interfaces to start. Check the official [Alpine.js](https://github.com/alpinejs/alpine) repository for more usage information.
