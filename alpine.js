'use strict'

class AlpineHelper {
  static async init () {
    Dlopen.register('alpine', {
      scripts: `https://cdn.jsdelivr.net/gh/alpinejs/alpine@v${ALPINE_VERSION}/dist/alpine.min.js`,
      init: () => {
        console.log('Loaded Alpine')
      }
    })

    Dlopen.register('alpine-helpers', {
      scripts: `https://cdn.jsdelivr.net/gh/alpine-collective/alpine-magic-helpers@${ALPINE_HELPERS_VERSION}/dist/index.min.js`,
      init: () => {
        console.log('Loaded Alpine Magic Helpers')
      }
    })
  }

  static async setup () {
    await Dlopen.loadDependencies(['alpine', 'alpine-helpers'])
    window.Alpine.start()

    console.log('Alpine.JS Initialized')
  }
}

Hooks.on('init', () => AlpineHelper.init())
Hooks.on('setup', () => AlpineHelper.setup())
